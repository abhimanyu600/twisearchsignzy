package signzy.twisearch.model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

/**
 * created by AbhimanyU on 24-02-2018.
 */
public class TweetStatuses {
    private final List<Status> statuses;

    public TweetStatuses(List<Status> statuses) {
        this.statuses = statuses;
    }

    public static class Status {
        @SerializedName("created_at")
        private final Date createdAt;

        @SerializedName("retweet_count")
        private final String retweetCount;

        private final String full_text;

        private final User user;

        private static class User {
            private final String name;
            @SerializedName("screen_name")
            private final String screenName;

            private User(String name, String screenName) {
                this.name = name;
                this.screenName = screenName;
            }

            public String getName() {
                return name;
            }

            public String getScreenName() {
                return screenName;
            }
        }

        public Status(Date createdAt, String retweetCount, String text, User user) {
            this.createdAt = createdAt;
            this.full_text = text;
            this.user = user;
            this.retweetCount = retweetCount;
        }

        public Date getCreatedAt() {
            return createdAt;
        }

        public String getRetweetCount() { return retweetCount; }

        public String getText() {
            return full_text;
        }

        public String getUserName() {
            return user.getName();
        }

        public String getUserScreenName() {
            return user.getScreenName();
        }
    }

    public List<Status> getStatuses() {
        return statuses;
    }
}

