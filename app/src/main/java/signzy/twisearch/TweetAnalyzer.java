package signzy.twisearch;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import signzy.twisearch.model.Tweet;
import signzy.twisearch.model.TweetRetweets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TweetAnalyzer extends AppCompatActivity {

    ArrayList<TweetRetweets> tweets;
    ArrayList<String> tweetDisplay;
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tweet_analyzer);

        lv = (ListView)findViewById(R.id.display_freq);
        tweetDisplay = new ArrayList<>();

        tweets = (ArrayList<TweetRetweets>)getIntent().getSerializableExtra("tweets");


        // Counting the sum of retweets

        for(int i=0;i<tweets.size();i++)
        {
            for(int j=1;j<tweets.size()-1;j++)
            {
                if(tweets.get(j-1).getRetweet_count() < tweets.get(j).getRetweet_count())
                {
                    TweetRetweets tmp = new TweetRetweets();
                    tmp.setAtCounts(tweets.get(j-1).getAtCounts());
                    tmp.setContent(tweets.get(j-1).getContent());
                    tmp.setRetweet_count(tweets.get(j-1).getRetweet_count());

                    tweets.get(j-1).setRetweet_count(tweets.get(j).getRetweet_count());
                    tweets.get(j-1).setContent(tweets.get(j).getContent());
                    tweets.get(j-1).setAtCounts(tweets.get(j).getAtCounts());

                    tweets.get(j).setRetweet_count(tmp.getRetweet_count());
                    tweets.get(j).setContent(tmp.getContent());
                    tweets.get(j).setAtCounts(tmp.getAtCounts());
                }
            }
        }

        // Counting the sum of mentions

        for(int i=0;i<tweets.size();i++)
        {
            for(int j=1;j<tweets.size()-1;j++)
            {
                if(tweets.get(j-1).getRetweet_count() == tweets.get(j).getRetweet_count())
                {
                    if(tweets.get(j-1).getAtCounts() < tweets.get(j).getAtCounts()) {
                        TweetRetweets tmp = new TweetRetweets();
                        tmp.setAtCounts(tweets.get(j - 1).getAtCounts());
                        tmp.setContent(tweets.get(j - 1).getContent());
                        tmp.setRetweet_count(tweets.get(j - 1).getRetweet_count());

                        tweets.get(j - 1).setRetweet_count(tweets.get(j).getRetweet_count());
                        tweets.get(j - 1).setContent(tweets.get(j).getContent());
                        tweets.get(j - 1).setAtCounts(tweets.get(j).getAtCounts());

                        tweets.get(j).setRetweet_count(tmp.getRetweet_count());
                        tweets.get(j).setContent(tmp.getContent());
                        tweets.get(j).setAtCounts(tmp.getAtCounts());
                    }
                }
            }
        }

        // Copying top 3 elements in tweetDisplay
        for (int i=0;i<tweets.size()-1;i++)
        {
            if(!tweets.get(i).getContent().equals(tweets.get(i+1).getContent()))
                tweetDisplay.add("Tweet: "+tweets.get(i).getContent()+"\nRetweets: "+tweets.get(i).getRetweet_count()+"\nAt Mentions: "+tweets.get(i).getAtCounts());
            if(tweetDisplay.size()==3)
                break;
        }

        //Displaying the list
        lv.setAdapter(new ArrayAdapter<String>(TweetAnalyzer.this,R.layout.row_tweet_item,R.id.tweet_full_text,tweetDisplay));
        Log.d("Tweets",tweets.toString());

    }
}

