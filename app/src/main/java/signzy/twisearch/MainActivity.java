package signzy.twisearch;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import signzy.twisearch.adapter.TweetAdapter;
import signzy.twisearch.model.Tweet;
import signzy.twisearch.model.TweetRetweets;
import signzy.twisearch.model.TweetStatuses;
import signzy.twisearch.network.ApiClient;
import signzy.twisearch.network.ApiInterface;
import signzy.twisearch.network.Oauth2Token;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    LinearLayout ll;
    RecyclerView.LayoutManager layoutManager;
    TweetAdapter tweetAdapter;
    List<Tweet> tweets;
    ArrayList<TweetRetweets> tweet_content;
    ApiInterface apiInterface;
    private String mConsumerKey;
    private String mConsumerSecret;
    Call<TweetStatuses> call;
    Call<Oauth2Token> mTokenCall;
    String access;
    ProgressDialog pd;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater in = getMenuInflater();
        in.inflate(R.menu.menu_main,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.top3)
        {
            try {
                if (tweet_content == null || tweet_content.size() <= 0)
                    Toast.makeText(MainActivity.this, "No tweets to analyse!", Toast.LENGTH_SHORT).show();
                else
                    startActivity(new Intent(MainActivity.this, TweetAnalyzer.class).putExtra("tweets", tweet_content));
            }catch (Exception e)
            {
                Log.d("Exception : menu_main",e.toString());
            }

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView)findViewById(R.id.tweet_holder);
        ll =(LinearLayout)findViewById(R.id.llayout);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        pd = new ProgressDialog(MainActivity.this);
        pd.setMessage("Fetching 'ClearTax' Tweets...");
        pd.setCancelable(false);
        pd.show();

        mConsumerKey = "FqmPVERR4WsPkGLv2zboHsyZr";
        mConsumerSecret ="eEqkqC5NN3EA3aAcd8LolmduAJYoKYsYd9AyEOdxqNBnaF77X8";

        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        mTokenCall = apiInterface.getAccessToken(getAuthorizationHeader(), "client_credentials");
        mTokenCall.enqueue(new Callback<Oauth2Token>() {
            @Override
            public void onResponse(Call<Oauth2Token> call, Response<Oauth2Token> response) {
                if (response.isSuccessful()) {
                    saveAccessToken(response.body().getAccessToken());
                }
            }

            @Override
            public void onFailure(Call<Oauth2Token> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Something went wrong !", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private int count(String text, char c)
    {
        int counter = 0;
        for( int i=0; i<text.length(); i++ ) {
            if( text.charAt(i) == c ) {
                counter++;
            }
        }
        return counter;
    }

    private void copyContent(List<Tweet> data){
        tweet_content = new ArrayList<>();
        for(int i=0;i<data.size();i++)
        {
            TweetRetweets tr = new TweetRetweets();
            tr.setContent(data.get(i).getContent());
            tr.setRetweet_count(Integer.parseInt(data.get(i).getRetweetCount()));
            tr.setAtCounts(count(data.get(i).getContent(),'@'));
            tweet_content.add(tr);
        }
    }

    private void saveAccessToken(String accessToken) {
        access = "Bearer " + accessToken;
        Log.d ("access",access);
        call = apiInterface.getTweets(access,"ClearTax","100","recent","extended");
        call.enqueue(new Callback<TweetStatuses>() {
            @Override
            public void onResponse(Call<TweetStatuses> call, Response<TweetStatuses> response) {
                if (response.isSuccessful()) {
                    Log.d("response",response.body().toString());
                    pd.dismiss();
                    ll.setVisibility(View.VISIBLE);
                    tweets = Tweet.buildTweets(response.body());
                    copyContent(tweets);
                    tweetAdapter = new TweetAdapter(tweets);
                    recyclerView.setAdapter(tweetAdapter);
                }
            }

            @Override
            public void onFailure(Call<TweetStatuses> call, Throwable t) {
                Log.d("Error","Something Happened !"+call.toString()+" and "+t.toString());
            }
        });
    }

    private String getAuthorizationHeader() {
        try {
            String consumerKeyAndSecret = mConsumerKey + ":" + mConsumerSecret;
            byte[] data = consumerKeyAndSecret.getBytes("UTF-8");
            String base64 = Base64.encodeToString(data, Base64.NO_WRAP);

            return "Basic " + base64;
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    private void safeStop(Call call) {
        if (call != null && !call.isCanceled()) {
            call.cancel();
        }
    }
}
