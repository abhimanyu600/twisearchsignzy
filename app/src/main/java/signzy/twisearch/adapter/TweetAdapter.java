package signzy.twisearch.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import signzy.twisearch.R;
import signzy.twisearch.model.Tweet;

import java.util.List;

/**
 * created by AbhimanyU on 25-02-2018.
 */
public class TweetAdapter extends RecyclerView.Adapter<TweetAdapter.TweetViewHolder> {

    private List<Tweet> tweets;

    public TweetAdapter (List<Tweet> tweets){
        this.tweets = tweets;
    }
    @Override
    public TweetViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_tweet_item,parent,false);
        return new TweetViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TweetViewHolder holder, int position) {
        //
        Tweet tItem = tweets.get(position);
        holder.tweet_text.setText("User: "+tweets.get(position).getUsername()+"\nTweet: "+tweets.get(position).getContent()+"\nPosted On: "+tweets.get(position).getCreatedAt()+"\nRetweet Count: "+tweets.get(position).getRetweetCount());
    }

    @Override
    public int getItemCount() {
        return tweets.size();
    }

    public class TweetViewHolder extends RecyclerView.ViewHolder
    {
        TextView tweet_text;

        public TweetViewHolder(View itemView) {
            super(itemView);
            tweet_text = (TextView)itemView.findViewById(R.id.tweet_full_text);
        }
    }
}
