package signzy.twisearch.network;

import signzy.twisearch.model.TweetStatuses;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * created by AbhimanyU on 24-02-2018.
 */
public interface ApiInterface {


    @POST("oauth2/token")
    @FormUrlEncoded
    Call<Oauth2Token> getAccessToken(@Header("Authorization") String authorization,
                                     @Field("grant_type") String grantType);

    @GET("1.1/search/tweets.json")
    Call<TweetStatuses> getTweets(@Header("Authorization") String authorization,
                                  @Query("q") String query, @Query("count") String count, @Query("result_type") String type, @Query("tweet_mode") String extended);
}
